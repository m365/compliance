﻿<#
.DESCRIPTION
    Required Modules:      Microsoft.Graph 

    Minumum Roles or Permissions:
        * GraphApi Scope:  User.Read.All,Device.Read.All,DeviceManagementManagedDevices.Read.All,BitLockerKey.Read.All,AuditLog.Read.All

.PARAMETER ImportFilePath
    Definuje cestu k csv souboru, ktery obsahuje seznam zarizeni, jejich aktivita ma byt auditovana.
    Zarizeni v souboru mohou byt zadany pomoci DeviceID nebo DisplayName. Na kazdem radku musi byt zadana pouze jedno zarizeni.
    Nelze pouzit soucasne s parametry: Devices a AllDevices
.PARAMETER Devices
    Rucne zadany seznam zarizeni, jejichz aktivita ma byt auditovana. Zarizeni lze zadat pomoci DeviceID nebo DisplayName.
    Zarizeni se oddeluji carkou a v pripade, ze DisplayName obsahuje mezeru nebo jiny znak, pak je nutne zadat nazev do uvozovek.
    Nelze pouzit soucasne s parametry: ImportFilePath a AllDevices
.PARAMETER AllDevices
    Pri pouziti tohoto parametru dojde k auditu vsech zarizeni.
    Nelze pouzit soucasne s parametry: ImportFilePath a Devices
.PARAMETER ExportBitLockerRecoveryKeys
    Vyexportuje seznam BitLocker klicu vsech zarizeni
.PARAMETER ExportDirectoryPath
    Cesta k vystupnimu souboru. Pokud bude zadana neplana, pak se vysledky ulozi do "$($env:HOMEDRIVE)\Temp"

.EXAMPLE
     Export-DeviceInfo.ps1 -AllDevices

.EXAMPLE
     Export-DeviceInfo.ps1 -Devices "3a6715e4-a6fc-4326-82f9-624cdb185216:6896158623514444","DESKTOP-7LBMOMR7"

.EXAMPLE
    Export-DeviceInfo.ps1 -ImportFilePath C:\Temp\importDevice.csv -ExportBitLockerRecoveryKeys -ExportDirectoryPath C:\Temp

.NOTES
    Date:   2024-01-30
	Author: Stanislav Kalina, stanislav.kalina@cvut.cz
#>

param(
    [Parameter(Position=0,ParameterSetName='ImportFilePath')][ValidateScript(
    {
        if (!(Test-Path $_ -PathType Leaf)) {throw "Input file ($($_)) does not exist !"}
        if ($_ -notmatch "\.csv$") { throw "The file specified in the path argument must be csv type !" }
        return $true
    })][array]$ImportFilePath,
    [Parameter(Position=0,ParameterSetName='Devices')][array]$Devices,
    [Parameter(Position=0,ParameterSetName='All')][switch][bool]$AllDevices,
    [Parameter(Position=1,Mandatory=$false)][switch][bool]$ExportBitLockerRecoveryKeys,
    [Parameter(Position=2,Mandatory=$false)][ValidateScript(
    {
        if (!(Test-Path $_ -PathType Container)) {throw "Output file ($($_)) does not exist !"}
        return $true
    })][string]$ExportDirectoryPath = "$($env:HOMEDRIVE)\Temp" 
)


[array]$activeIdentityAttribute = @('extension_6511fd4bb0bc4f8694f52b14c8c599ad_cvutPerson') ### Nazev atributu v EntraID, ktery urcuje, jestli ma clen skupiny aktivni vztah k univerzite. Pokud neni nastavena zadna hodnota, pak nebudou ve vysledcich zobrazeny hodnoty ve sloupci ActiveOwner
[int32]$newDeviceDays = 7 ### Definuje pocet dni od zalozeni, kdy bude skupina v exportu oznacena jako nova

[datetime]$dateTime = Get-Date
[string]$dateTimeFileFormat = Get-Date $dateTime -Format yyyyMMddHHmmss

[datetime]$auditLogStartDate = $dateTime.AddDays(-$auditLogDays)
[datetime]$auditLogEndDate = $dateTime

$ExportDirectoryPath = $ExportDirectoryPath.TrimEnd('\').TrimEnd('/')
$ExportFilePath = "$ExportDirectoryPath\ExportDevice-$dateTimeFileFormat.csv"
$ExportKeysFilePath = "$ExportDirectoryPath\ExportDeviceKeys-$dateTimeFileFormat.csv"

### Vlastnosti, ktere budou nacitany a exportovany:
$mgUserProperties = @('Id','UserPrincipalName','DisplayName','UserType','AccountEnabled','OnPremisesSyncEnabled','CreatedDateTime','Mail','ProxyAddresses','SignInActivity') ### Get-MgUser
$mgUserSignInProperties = @('LastSignInDateTime','LastNonInteractiveSignInDateTime') ### Get-MgUser
$mgDeviceProperties = @('AccountEnabled','ApproximateLastSignInDateTime','DeviceId','DisplayName','Id','IsCompliant','IsManaged','MdmAppId','OperatingSystem','OperatingSystemVersion','PhysicalIds','ProfileType','RegistrationDateTime','TrustType') ### Get-MgDevice
$intuneDeviceProperties = @('id','userId','deviceName','managedDeviceOwnerType','enrolledDateTime','lastSyncDateTime','operatingSystem','complianceState','jailBroken','managementAgent','osVersion','easActivated','easDeviceId','easActivationDateTime','azureADRegistered','deviceEnrollmentType','emailAddress','azureADDeviceId','deviceRegistrationState','isEncrypted','userPrincipalName','model','manufacturer','imei','complianceGracePeriodExpirationDateTime','serialNumber','wiFiMacAddress','managementCertificateExpirationDate') ### Get-MgDeviceManagementManagedDevice
$recoveryKeyProperties = @('CreatedDateTime','DeviceId','Id','Key','VolumeType')

$guidPattern= "\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"
$mgUserFilter = "UserType eq 'Member'"



if ([bool]$ImportFilePath) { [array]$Devices = Get-Content $ImportFilePath }



$error.Clear()

Write-Host "GraphApi Connection..."
Connect-MgGraph -Scopes User.Read.All,Device.Read.All,DeviceManagementManagedDevices.Read.All,BitLockerKey.Read.All,AuditLog.Read.All -NoWelcome

if ([bool]$error.Count) { BREAK }



$entraDevices = @()
$intuneDevices = @{} ### Vsechna zarizeni s vazbou na objekt v Entra ID
$intuneDevicesMissingEntraID = @() ### Vsechna zarizeni bez vazby na objekt v Entra ID
$recoveryKeys = @{}
$activeUsers = @{}
$entraUsers = @{}

if ([bool]$Devices.Count) {
    foreach ($device in $Devices) {
        if ($device -match $guidPattern) { $deviceFilter = "DeviceId eq '$device'" }
        else { $deviceFilter = "DisplayName eq '$device'" }

        $entraDvcs = Get-MgDevice -Filter $deviceFilter -Property $mgDeviceProperties | Select-Object -Property $mgDeviceProperties
        if ([bool]$entraDvcs) {
            foreach ($entraDevice in $entraDvcs) {
                $entraDevices += $entraDevice
                Get-MgDeviceManagementManagedDevice -Filter "azureADDeviceId eq '$($entraDevice.DeviceId)'" | ForEach-Object { if ($_.azureADDeviceId -like "00000000-0000-0000-0000-000000000000") { $intuneDevicesMissingEntraID += $_ } else { $intuneDevices.Add($_.azureADDeviceId,$_) } }
                Get-MgInformationProtectionBitlockerRecoveryKey -Filter "DeviceId eq '$($entraDevice.DeviceId)'" | Select-Object -Property $recoveryKeyProperties | Group-Object -Property DeviceId | ForEach-Object { $recoveryKeys.Add($_.Name,$_.Group) }
                
                $userGUID = [Regex]::Matches($entraDevice.PhysicalIds -match "USER-GID", $guidPattern).Value
                if (![bool]$userGUID) {
                    $userGUID = (Get-MgDeviceRegisteredOwner -DeviceId $entraDevice.Id).Id
                    if (![bool]$userGUID) { $userGUID = (Get-MgDeviceManagementManagedDevice -Filter "azureADDeviceId eq '$($entraDevice.DeviceId)'").UserId }
                }

                if ([bool]$userGUID -and !$entraUsers.ContainsKey($userGUID)) { Get-MgUser -Filter "Id eq '$userGUID'" -Property ($mgUserProperties+$activeIdentityAttribute) | Select-Object -Property ($mgUserProperties+'AdditionalProperties') |  ForEach-Object { $entraUsers.Add($_.Id,$_); $activeUsers.Add($_.UserPrincipalName,$_.AdditionalProperties."$activeIdentityAttribute") } }
            }
        }
    }
    if ([bool]$entraDevices.Count) { Write-Host "Loaded $($entraDevices.Count) Entra Devices !" } else { BREAK }
    if ([bool]$intuneDevices.Count) { Write-Host "Loaded $($intuneDevices.Count) Intune Devices !" }
    if ([bool]$intuneDevicesMissingEntraID.Count) { Write-Host "Loaded $($intuneDevicesMissingEntraID.Count) Intune Devices with Missing Entra GUID !" }
    if ([bool]$recoveryKeys.Count) { Write-Host "Loaded $($recoveryKeys.Count) Devices with BitLocker Recovery Keys !" }
    if ([bool]$entraUsers.Count) { Write-Host "Loaded $($entraUsers.Count) Entra Users !" }
}
else {
    ### Nacteni vsech zarizeni v Entra ID (60 000 objektu cca. 3 min.)
    $entraDevices = Get-MgDevice -All -Property $mgDeviceProperties | Select-Object -Property $mgDeviceProperties
    if ([bool]$entraDevices.Count) { Write-Host "Loaded $($entraDevices.Count) Entra Devices !" } else { BREAK }

    ### Nacteni vsech zarizeni v Intune (2 000 objektu cca. 10 sec.)
    Get-MgDeviceManagementManagedDevice -All | Select-Object -Property $intuneDeviceProperties | ForEach-Object { if ($_.azureADDeviceId -like "00000000-0000-0000-0000-000000000000") {$intuneDevicesMissingEntraID += $_} else { $intuneDevices.Add($_.azureADDeviceId,$_) } }
    if ([bool]$intuneDevices.Count) { Write-Host "Loaded $($intuneDevices.Count) Intune Devices !" } else { BREAK }
    if ([bool]$intuneDevicesMissingEntraID.Count) { Write-Host "Loaded $($intuneDevicesMissingEntraID.Count) Intune Devices with Missing Entra GUID !" } else { BREAK }

    ### Nacteni vsech zarizeni v Entra ID (8 000 objektu cca. 8 min.)
    Get-MgInformationProtectionBitlockerRecoveryKey -All | Select-Object -Property $recoveryKeyProperties | Group-Object -Property DeviceId | ForEach-Object { $recoveryKeys.Add($_.Name,$_.Group) }
    if ([bool]$recoveryKeys.Count) { Write-Host "Loaded $($recoveryKeys.Count) Devices with BitLocker Recovery Keys !" } else { BREAK }

    ### Nacteni vsech identit v Entra ID (200 000 objektu cca. 16 min.)
    Get-MgUser -All -Filter $mgUserFilter -Property ($mgUserProperties+$activeIdentityAttribute) | Select-Object -Property ($mgUserProperties+'AdditionalProperties') |  ForEach-Object { $entraUsers.Add($_.Id,$_); $activeUsers.Add($_.UserPrincipalName,$_.AdditionalProperties."$activeIdentityAttribute") }
    if ([bool]$entraUsers.Count) { Write-Host "Loaded $($entraUsers.Count) Entra Users !" } else { BREAK }
}


### Spojeni vsech vlastnosti, ktere se pouziji jako sloupce v tabulce $exportDevicesData
### K nekterym vlastnostem se pridavaji prefixy, aby bylo jasne definovane, k cemu se vztahuji
$allColumns = @('ORG_NewDevice')
$mgDeviceProperties | foreach { $allColumns += "EntraDevice $($_)" }
$intuneDeviceProperties | foreach { $allColumns += "IntuneDevice $($_)" }
$mgUserProperties -notmatch 'SignInActivity' | foreach { $allColumns += "EntraUser $($_)" }
$mgUserSignInProperties | foreach { $allColumns += "EntraUser $($_)" }
$allColumns += "ActiveOwner","ContainsBitlockerKeys","BitlockerKeys"

### Zalozeni tabulky, do ktere se budou zapisovat informace o zarizenich a ktera bude na konci skriptu exportovana do vystupniho souboru
$exportDevicesData = New-Object System.Data.DataTable
foreach ($columnName in $allColumns) { $exportDevicesData.Columns.Add($columnName, 'System.String') | Out-Null }


if ($ExportBitLockerRecoveryKeys) {
    ### Zalozeni tabulky, do ktere se budou zapisovat informace o Bitlocker klicich a ktera bude na konci skriptu exportovana do vystupniho souboru
    $exportKeys = New-Object System.Data.DataTable
    foreach ($columnName in $recoveryKeyProperties) { $exportKeys.Columns.Add($columnName, 'System.String') | Out-Null }
}



$entraDeviceNo = 0

### Hlavni smycka
foreach ($entraDevice in $entraDevices) {

    ### ProgressBar
    $entraDeviceNo++
    $ProgressBar = "Processing $entraDeviceNo of $($entraDevices.Count): $($entraDevice.DisplayName)"  
    Write-Progress -Activity "Analyzing EntraDevices activity:" -Status $ProgressBar -PercentComplete ($entraDeviceNo/$entraDevices.Count*100)

    $row = $exportDevicesData.NewRow() ### Vytvoreni noveho radku do exportni tabulky

    if ($entraDevice.CreatedDateTime -gt $dateTime.AddDays(-$newDeviceDays)) { $row.ORG_NewDevice = 'True' }
    
    $entraDeviceId = $entraDevice.DeviceId

    foreach ($property in $mgDeviceProperties) {
        if ([bool]$entraDevice.$property) {
            $rowProperty = "EntraDevice $property"
            if ($entraDevice.$property.GetType().BaseType.Name -eq 'Array') { if ([bool]$entraDevice.$property) { $row.$rowProperty = $entraDevice.$property -join ';' } }
            else { $row.$rowProperty = $entraDevice.$property }
        }
    }

    $userGUID = [Regex]::Matches($entraDevice.PhysicalIds -match "USER-GID", $guidPattern).Value
    if ([bool]$userGUID) { $entraDeviceOwner = $entraUsers.$userGUID }
    else {
        ### Pokud neni u objektu USER-GID, pak jej se jej pokusi dohledat naprimo
        $userGUID = (Get-MgDeviceRegisteredOwner -DeviceId $entraDevice.Id).Id
        if ([bool]$userGUID) { $entraDeviceOwner = $entraUsers.$userGUID }
        elseif ([bool]$intuneDevices.$entraDeviceId.UserId) { $entraDeviceOwner = $entraUsers.($intuneDevices.$entraDeviceId.UserId) } ### Jestlize se vyhledavani nezdari, zkontroluje se, ze informace o vlastnikovi neni ulozena v Intune
        else { $entraDeviceOwner = "" }
    }

    if ([bool]$entraDeviceOwner) {

        foreach ($property in ($mgUserProperties -notmatch 'SignInActivity')) {
            if ([bool]$entraDeviceOwner.$property) {
                $rowProperty = "EntraUser $property"
                if ($entraDeviceOwner.$property.GetType().BaseType.Name -eq 'Array') { if ([bool]$entraDeviceOwner.$property) { $row.$rowProperty = $entraDeviceOwner.$property -join ';' } }
                else { $row.$rowProperty = $entraDeviceOwner.$property }
            }
        }

        foreach ($property in $mgUserSignInProperties) {
            if ([bool]$entraDeviceOwner.'SignInActivity'.$property) {
                $rowProperty = "EntraUser $property"
                $row.$rowProperty = $entraDeviceOwner.'SignInActivity'.$property
            }
        }

        if ([bool]$activeUsers.($entraDeviceOwner.UserPrincipalName)) { $row.ActiveOwner = 'True' }  else { $row.ActiveOwner = 'False' }
    } else { $row.'EntraUser Id' = 'NA'; $row.'EntraUser UserPrincipalName' = 'NA' }

    if ([bool]$intuneDevices.$entraDeviceId) {
        
        $intuneDevice = $intuneDevices.$entraDeviceId

        foreach ($property in $intuneDeviceProperties) {
            if ([bool]$intuneDevice.$property) {
                $rowProperty = "IntuneDevice $property"
                $row.$rowProperty = $intuneDevice.$property
            }
        }

    }

    if ($recoveryKey = $recoveryKeys.$entraDeviceId) {
        $row.ContainsBitlockerKeys = "True"
        $row.BitlockerKeys = $recoveryKey.Count

        if ($ExportBitLockerRecoveryKeys) {
            foreach ($key in $recoveryKey) {
                $rowKey = $exportKeys.NewRow()
                foreach ($property in $recoveryKeyProperties) {
                    if ($property -eq "Key") {$rowKey.Key = (Get-MgInformationProtectionBitlockerRecoveryKey -BitlockerRecoveryKeyId $rowKey.Id -Property Key).Key }
                    else { $rowKey.$Property = $key.$property }
                }
                $exportKeys.Rows.Add($rowKey) ### Ulozeni informaci o Bitlocker klicich do exportni tabulky
            }
        }
    }

    $exportDevicesData.Rows.Add($row) ### Ulozeni informaci o zarizeni do exportni tabulky
}



### 
if ([bool]$intuneDevicesMissingEntraID) {

    $intuneDeviceNo = 0
    
    foreach ($intuneDevice in $intuneDevicesMissingEntraID) {
        
        ### ProgressBar
        $intuneDeviceNo++
        $ProgressBar = "Processing $intuneDeviceNo of $($intuneDevicesMissingEntraID.Count): $($intuneDevice.DisplayName)"  
        Write-Progress -Activity "Analyzing IntuneDevices with Missing Entra GUID activity:" -Status $ProgressBar -PercentComplete ($intuneDeviceNo/$intuneDevicesMissingEntraID.Count*100)
    
        $row = $exportDevicesData.NewRow()

        $row.'EntraDevice DeviceId' = 'NA'
        $row.'EntraDevice DisplayName' = 'NA'
        $row.'EntraDevice Id' = 'NA'
    
        foreach ($property in $intuneDeviceProperties) {
            if ([bool]$intuneDevice.$property) {
                $rowProperty = "IntuneDevice $property"
                $row.$rowProperty = $intuneDevice.$property
            }
        }

        if ([bool]$intuneDevice.UserPrincipalName) {
            if ([bool]$activeUsers.($intuneDevice.UserPrincipalName)) { $row.ActiveOwner = 'True' } else { $row.ActiveOwner = 'False' }
        } else { $row.'EntraUser Id' = 'NA'; $row.'EntraUser UserPrincipalName' = 'NA' }
    
        $exportDevicesData.Rows.Add($row)
    }
}

Write-Host "Exporting Data..."
if ($ExportBitLockerRecoveryKeys -and [bool]$exportKeys) { $exportKeys | Export-Csv -Delimiter ',' -Path $ExportKeysFilePath -Encoding UTF8 -NoTypeInformation }

$exportDevicesData | Export-Csv -Delimiter ',' -Path $ExportFilePath -Encoding UTF8 -NoTypeInformation

