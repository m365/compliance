﻿<#
.DESCRIPTION
    Required Modules:      ExchangeOnlineManagement, Microsoft.Graph 

    Minumum Roles or Permissions:
        * ExchangeOnline:  View-Only Audit Logs/Audit Logs/Information Protection Analyst/Information Protection Investigator
                           View-Only Recipients/Mail Recipients
        * GraphApi Scope:  GroupMember.Read.All,Reports.Read.All,Member.Read.Hidden,AuditLog.Read.All

    Upozorneni: Do pameti se nacita velke mnozstvi informaci, proto je nutne mit alokovanu dostatecnou kapacitu.
                Napr. 35 000 skupin, 35 000 uzivatelu cca. 10 GB RAM

.PARAMETER ImportFilePath
    Definuje cestu k csv souboru, ktery obsahuje seznam skupin, jejich aktivita ma byt auditovana.
    Skupiny v souboru mohou byt zadany pomoci GroupID nebo DisplayName. Na kazdem radku musi byt zadana pouze jedna skupina.
    Nelze pouzit soucasne s parametry: Groups a GroupType
.PARAMETER Groups
    Rucne zadany seznam skupin, jejich aktivita ma byt auditovana. Skupiny lze zadat pomoci GroupID nebo DisplayName.
    Skupiny se oddeluji carkou a v pripade, ze DisplayName obsahuje mezeru nebo jiny znak, pak je nutne zadat nazev do uvozovek.
    Nelze pouzit soucasne s parametry: ImportFilePath a GroupType
.PARAMETER GroupType
    Definuje filtr, ktery urcuje, jaky typ skupin bude auditovan:
    * All - vsechny skupiny
    * MailEnabled - Distribution lists, Microsoft 365 groups, Mail-enabled security groups
    * Microsoft365 - Microsoft 365 groups
    * TeamEnabled - Team-enabled Microsoft 365 groups
    * Security - Security groups, Mail-enabled security groups
    Nelze pouzit soucasne s parametry: ImportFilePath a Groups
.PARAMETER IncludeUsageReports
    Do vystupu zahrne i informace z UsageReports
.PARAMETER IncludeAuditLogs
    Do vystupu zahrne i informace o SharePointFileOperation z AuditLogs.
    Rozsah auditovani lze upravit v promenne $auditLogRecordType
    Prochazeni auditnich logu vyrazne prodluzuje beh skriptu !!!
.PARAMETER IncludeEXOMailboxStat
    Do vystupu zahrne i informace o schrance a o slozkach: Inbox a TeamsMessagesData
    Zahrnuti techtno informaci do auditu prodluzuje beh skriptu !!!
.PARAMETER IncludeMembers
    Do vystupu zahrne aktualni pocet clenu a UPN vsech vlastniku skupiny.
    Nacteni vsech clenu a vlastniku prodluzuje beh scriptu v zavislosti na poctu skupin a mnozstvi clenu.
    Poku je ale definovana promenna $activeIdentityAttribute, pak do vystupu zahrne i pocty aktivnich clenu a vlastniku.
    Promenna $activeIdentityAttribute obsahuje nazev atributu, ktery oznacuje aktivni identity v IS univerzity.
.PARAMETER ExportGroupMembers
    Vyexportuje seznam UPN vsech clenu a vlastniku
.PARAMETER ExportDirectoryPath
    Cesta k vystupnimu souboru. Pokud bude zadana neplana, pak se vysledky ulozi do "$($env:HOMEDRIVE)\Temp"

.EXAMPLE
    Export-GroupInfo.ps1 -GroupType TeamEnabled -IncludeUsageReports -ExportDirectoryPath C:\Temp
    Popis:
    Nacte informace pouze o tech skupinach, jejichz soucasti je Teams. Informace o aktivite nacte pouze z Usage Reports.
    Vystupem je soubor obsahujici podrobne informace o aktivite vsech skupin, jejichz soucasti je Teams.
    Ostatni Microsoft 365 skupiny nebudou soucasti reportu.

.EXAMPLE
    Export-GroupInfo.ps1 -ImportFilePath C:\Temp\importGroups.csv -IncludeUsageReports -IncludeAuditLogs -IncludeEXOMailboxStat -IncludeMembers -ExportGroupMembers -ExportDirectoryPath C:\Temp
    Popis:
    Provede nacteni vybranych skupin ze vstupniho souboru a vyhodnoceni jejich aktivity ze vsech zdroju.
    Vystupem je soubor obsahujici seznam skupin a jejich clenu a vlastniku a soubor obsahujici podrobne informace o aktivite vsech skupin nactenych ze vstupniho souboru.

.NOTES
    Date:   2024-01-30
	Author: Stanislav Kalina, stanislav.kalina@cvut.cz
#>

param(
    [Parameter(Position=0,Mandatory=$true,ParameterSetName='ImportFilePath')][ValidateScript(
    {
        if (!(Test-Path $_ -PathType Leaf)) {throw "Input file ($($_)) does not exist !"}
        if ($_ -notmatch "\.csv$") { throw "The file specified in the path argument must be csv type !" }
        return $true
    })][array]$ImportFilePath,
    [Parameter(Position=0,Mandatory=$true,ParameterSetName='Groups')][array]$Groups,
    [Parameter(Position=0,Mandatory=$true,ParameterSetName='GroupType')][ValidateSet('All','MailEnabled','Microsoft365','TeamEnabled','Security')][string]$GroupType,
    [Parameter(Position=1,Mandatory=$false)][switch][bool]$IncludeUsageReports,
    [Parameter(Position=2,Mandatory=$false)][switch][bool]$IncludeAuditLogs,
    [Parameter(Position=3,Mandatory=$false)][switch][bool]$IncludeEXOMailboxStat,
    [Parameter(Position=4,Mandatory=$false)][switch][bool]$IncludeMembers,
    [Parameter(Position=5,Mandatory=$false)][switch][bool]$ExportGroupMembers,
    [Parameter(Position=6,Mandatory=$false)][ValidateScript(
    {
        if (!(Test-Path $_ -PathType Container)) {throw "Output file ($($_)) does not exist !"}
        return $true
    })][string]$ExportDirectoryPath = "$($env:HOMEDRIVE)\Temp" 
)



[array]$activeIdentityAttribute = @('extension_6511fd4bb0bc4f8694f52b14c8c599ad_cvutPerson') ### Nazev atributu v EntraID, ktery urcuje, jestli ma clen skupiny aktivni vztah k univerzite. Pokud neni nastavena zadna hodnota, pak nebudou ve vysledcich zobrazeny hodnoty ve sloupcich ActiveMembersCount,ActiveOwnersCount
[string]$auditLogRecordType = 'SharePointFileOperation' ### Aktivity, ktere se vyhledavaji v auditnim logu. Lze zmenit: https://learn.microsoft.com/en-us/office/office-365-management-api/office-365-management-activity-api-schema#auditlogrecordtype
[int32]$auditLogResultSize = 100 ### Specifikuje maximalni pocer vysledku z auditniho logu. Maximalni hodnota je 5000.
[int32]$auditLogDays = 180 ### Urcuje, kolik dni do minulosti se maji prohledavat auditni logy
[int32]$newGroupDays = 7 ### Definuje pocet dni od zalozeni, kdy bude skupina v exportu oznacena jako nova
[string]$reportActivityPeriod = 'D180' ### Definuje jestli se maji pouzit reporty za poslednich 7, 30, 90 nebo 180 dni. Moznosti: D7/D30/D90/D180
[string]$excludeGroupFilter = "^All Users$|^GRP |^\d{2}000 SUMA" ### Filtr, ktery urcuje DisplayName skupin, ktere se nebudou analyzovany a nebudou se tak zobrazovat ve vysledcich



[datetime]$dateTime = Get-Date
[string]$dateTimeFileFormat = Get-Date $dateTime -Format yyyyMMddHHmmss

[datetime]$auditLogStartDate = $dateTime.AddDays(-$auditLogDays)
[datetime]$auditLogEndDate = $dateTime

$ExportDirectoryPath = $ExportDirectoryPath.TrimEnd('\').TrimEnd('/')
$ExportFilePath = "$ExportDirectoryPath\ExportGroup-$dateTimeFileFormat.csv"
$ExportMembersFilePath = "$ExportDirectoryPath\ExportGroupMembers-$dateTimeFileFormat.csv"

$reportOffice365GroupActivityFilePath = "$ExportDirectoryPath\MgReportOffice365GroupActivityDetail-$dateTimeFileFormat.csv"
$reportTeamActivityFilePath = "$ExportDirectoryPath\MgReportTeamActivityDetail-$dateTimeFileFormat.csv"
$reportSharePointSiteUsageFilePath = "$ExportDirectoryPath\MgReportSharePointSiteUsageDetail-$dateTimeFileFormat.csv"

### Vlastnosti, ktere budou nacitany a exportovany:
$mgGroupProperties = @('CreatedDateTime','Description','DisplayName','GroupTypes','Id','Mail','MailEnabled','MailNickname','OnPremisesLastSyncDateTime','OnPremisesSyncEnabled','resourceProvisioningOptions','SecurityEnabled','SecurityIdentifier','RenewedDateTime','Visibility') ### Get-MgGroup
$reportGroupProperties = @('Owner Principal Name','Last Activity Date','Member Count','External Member Count','Exchange Received Email Count','SharePoint Active File Count','Exchange Mailbox Total Item Count','SharePoint Total File Count') ### Get-MgReportOffice365GroupActivityDetail
$reportTeamProperties = @('Last Activity Date','Active Users','Active Channels','Channel Messages','Reactions','Meetings Organized','Post Messages','Reply Messages','Urgent Messages','Mentions','Guests','Active Shared Channels','Active External Users') ### Get-MgReportTeamActivityDetail
$reportSharePointProperties = @('Site Id','Site URL','Owner Display Name','Last Activity Date','File Count','Active File Count','Page View Count','Visited Page Count','Storage Used (Byte)','Root Web Template','Owner Principal Name') ### Get-MgReportSharePointSiteUsageDetail
$mailboxStatProperties = @('ItemCount','LastInteractionTime') ### Get-MailboxStatistics
$mailboxFolderProperties = @('LastModifiedTime','ItemsInFolder','OldestItemReceivedDate','NewestItemReceivedDate','OldestItemLastModifiedDate','NewestItemLastModifiedDate') ### Get-ExoMailboxFolderStatistics
$auditLogProperties = @('SPO AuditLogRecordsCount','SPO LastAuditLogDateTime','SPO UserIds','SPO LastAuditLogRecordType') ### Search-UnifiedAuditLog
$usersProperties = @('MembersCount','ActiveMembersCount','OwnersCount','ActiveOwnersCount','Owners')
$groupMembersProperties = @('GroupId','Owners','Members')

[string]$guidPattern = '\w{8}-\w{4}-\w{4}-\w{4}-\w{12}'


if ([bool]$ImportFilePath) { [array]$Groups = Get-Content $ImportFilePath }



$error.Clear()

Write-Host "GraphApi Connection..."
Connect-MgGraph -Scopes GroupMember.Read.All,Reports.Read.All,Member.Read.Hidden,AuditLog.Read.All -NoWelcome
if ($IncludeEXOMailboxStat -or $IncludeAuditLogs) {
    Write-Host "ExchangeOnline Connection..."
    Connect-ExchangeOnline -ShowBanner:$false
}

if ([bool]$error.Count) { BREAK }



if ($ExportGroupMembers) { $IncludeMembers = $true}

if ($IncludeMembers) { $groupExpandProperty = "Owners" }
else { $groupExpandProperty = $null }


[hashtable]$groupMembers = @{}
[hashtable]$groupOwners = @{}
[array]$mgGroups = @()


if ([bool]$Groups.Count) {
    foreach ($group in $Groups) {
        if ($group -match $guidPattern) { $groupFilter = "Id eq '$group'" }
        else { $groupFilter = "DisplayName eq '$group'" }
        
        ### Nacteni vlastniku/clenu v tomto kroku je casove nejefektivnejsi. Nacte ale max. 20 identit. Zbytek se doresi pozdeji pri behu hlavni smycky
        $mgGrps = Get-MgGroup -Filter $groupFilter -ExpandProperty $groupExpandProperty
        if ([bool]$mgGrps) {
            foreach ($mgGroup in $mgGrps) {
                $mgGroups += $mgGroup
                if ($IncludeMembers) {
                    $groupOwners.Add($mgGroup.Id,$mgGroup.Owners)
                    Get-MgGroup -Filter "Id eq '$($mgGroup.Id)'" -ExpandProperty Members | ForEach-Object { $groupMembers.Add($_.Id,$_.Members) }
                }
            }
        }
    }
}
else {
    switch ($GroupType) {
        MailEnabled { $groupTypeFilter = "mailEnabled eq true" }
        Microsoft365 { $groupTypeFilter = "groupTypes/any(c:c eq 'Unified')" }
        TeamEnabled { $groupTypeFilter = "resourceProvisioningOptions/Any(x:x eq 'Team')" }
        Security { $groupTypeFilter = "securityEnabled eq true" }
        default { $groupTypeFilter = $null }
    }
    
    ### Nacteni vlastniku/clenu v tomto kroku je casove nejefektivnejsi. Nacte ale max. 20 identit. Zbytek se doresi pozdeji pri behu hlavni smycky
    $mgGroups = Get-MgGroup -Filter $groupTypeFilter -All -ExpandProperty $groupExpandProperty

    if ($IncludeMembers) {
        foreach ($group in $mgGroups) { $groupOwners.Add($group.Id,$group.Owners) }
        Get-MgGroup -Filter $groupTypeFilter -All -ExpandProperty Members | ForEach-Object { $groupMembers.Add($_.Id,$_.Members) }
    }
}

if ([bool]$mgGroups.Count) { Write-Host "Loaded $($mgGroups.Count) Groups !" } else { BREAK }

### Ulozeni a nacteni vsech Usage Reportu
if ($IncludeUsageReports) {
    Get-MgReportOffice365GroupActivityDetail -Period $reportActivityPeriod -OutFile $reportOffice365GroupActivityFilePath
    if ($?) {
        if (Test-Path $reportOffice365GroupActivityFilePath) {
            $mgReportOffice365GroupActivityDetail = @{}
            Import-Csv $reportOffice365GroupActivityFilePath | ForEach-Object { $mgReportOffice365GroupActivityDetail.Add($_."Group Id",$_) }
        }
    }    
    
    Get-MgReportTeamActivityDetail -Period $reportActivityPeriod -OutFile $reportTeamActivityFilePath
    if ($?) {
        if (Test-Path $reportTeamActivityFilePath) {
            $mgReportTeamActivityDetail = @{}
            Import-Csv $reportTeamActivityFilePath | ForEach-Object { $mgReportTeamActivityDetail.Add($_."Team Id",$_) }
        }
    }

    Get-MgReportSharePointSiteUsageDetail -Period $reportActivityPeriod -OutFile $reportSharePointSiteUsageFilePath
    if ($?) {
        if (Test-Path $reportSharePointSiteUsageFilePath) {
            $mgReportSharePointSiteUsageDetail = @{}
            Import-Csv $reportSharePointSiteUsageFilePath | ForEach-Object { $mgReportSharePointSiteUsageDetail.Add($_."Site Id",$_) }
        }
    }
}



### Spojeni vsech vlastnosti, ktere se pouziji jako sloupce v tabulce $exportGroupActivity
### K nekterym vlstnostem se pridavaji prefixy, aby bylo jasne definovane, k cemu se vztahuji
$allColumns = @('ORG_NewGroup')
$allColumns += $mgGroupProperties
if ($IncludeMembers) { $allColumns += $usersProperties }
if ($IncludeUsageReports) {
    $reportGroupProperties | foreach { $allColumns += "GRP $($_)" }
    $reportTeamProperties | foreach { $allColumns += "TEAM $($_)" }
    $reportSharePointProperties | foreach { $allColumns += "SPO $($_)" }
}
if ($IncludeEXOMailboxStat) {
    $mailboxStatProperties | foreach { $allColumns += "MBX $($_)" }
    $mailboxFolderProperties | foreach { $allColumns += "MBX INBOX FOLDER $($_)" }
    $mailboxFolderProperties | foreach { $allColumns += "MBX TEAM FOLDER $($_)" }
}
if ($IncludeAuditLogs) { $allColumns += $auditLogProperties }

### Zalozeni tabulky, do ktere se budou zapisovat informace o skupinach a ktera bude na konci skriptu exportovana do vystupniho souboru
$exportGroupActivity = New-Object System.Data.DataTable
foreach ($columnName in $allColumns) { $exportGroupActivity.Columns.Add($columnName, 'System.String') | Out-Null }

if ($ExportGroupMembers) {
    ### Zalozeni tabulky, do ktere se budou zapisovat informace o clenech skupin a ktera bude na konci skriptu exportovana do vystupniho souboru
    $exportMembers = New-Object System.Data.DataTable
    foreach ($columnName in $groupMembersProperties) { $exportMembers.Columns.Add($columnName, 'System.String') | Out-Null }
}



$mgGroupNo = 0

### Hlavni smycka
foreach ($mgGroup in $mgGroups) {

    ### ProgressBar
    $mgGroupNo++
    $ProgressBar = "Processing $mgGroupNo of $($mgGroups.Count): $($mgGroup.DisplayName)"  
    Write-Progress -Activity "Analyzing MgGroup activity:" -Status $ProgressBar -PercentComplete ($mgGroupNo/$mgGroups.Count*100)
    
    if ($mgGroup.DisplayName -match $excludeGroupFilter) { continue } ### Pokud je nazev definovany v promenne $excludeGroupFilter, pak bude aktualni beh smycky ukoncen

    $row = $exportGroupActivity.NewRow() ### Vytvoreni noveho radku do exportni tabulky

    if ($mgGroup.CreatedDateTime -gt $dateTime.AddDays(-$newGroupDays)) { $row.ORG_NewGroup = 'True' }

    foreach ($property in $mgGroupProperties) {
        if ($property -eq 'resourceProvisioningOptions') { $row.$property = [string]($mgGroup.AdditionalProperties.resourceProvisioningOptions) }
        elseif ([bool]$mgGroup.$property) {
            if ($mgGroup.$property.GetType().BaseType.Name -eq 'Array') {
                if ([bool]$mgGroup.$property) { $row.$property = $mgGroup.$property -join ';' }
            }
            else {
                if ($property -eq 'Description') { $row.$property = $mgGroup.$property.Replace("`n",", ").Replace("`r",", ") } ### DisplayName nekterych skupin je viceradkovy!!!
                else { $row.$property = $mgGroup.$property }
            }
        }
    }
    
    if ($IncludeMembers) {
        [array]$members = $groupMembers.($mgGroup.Id)
        [array]$owners = $groupOwners.($mgGroup.Id)

        ### Pokud bylo nasteno pouze 20 clenu/vlastniku. Pak je nutne je znovu nacist. Pravdepodobne jich bude vice
        if ($members.Count -ge 20) { $members = Get-MgGroupMember -GroupId $mgGroup.Id -All -Property Id,userPrincipalName,$activeIdentityAttribute }

        $row.MembersCount = $members.Count
        if ([bool]$activeIdentityAttribute) {
           if ([bool]$members.Count) {
               if ([bool]$members.AdditionalProperties."$activeIdentityAttribute") { $row.ActiveMembersCount = ($members.AdditionalProperties."$activeIdentityAttribute" -match $true).Count }
               else { $row.ActiveMembersCount = 0 }
           } else { $row.ActiveMembersCount = 0 }
        } else { $row.ActiveMembersCount = 'NA' }

        ### Pokud bylo nasteno pouze 20 clenu/vlastniku. Pak je nutne je znovu nacist. Pravdepodobne jich bude vice
        if ($owners.Count -ge 20) { $owners = Get-MgGroupOwner -GroupId $mgGroup.Id -All -Property Id,userPrincipalName,$activeIdentityAttribute }
        
        $row.OwnersCount = $owners.Count
        if ([bool]$activeIdentityAttribute) {
            if ([bool]$owners.Count) {
                if ([bool]$owners.AdditionalProperties."$activeIdentityAttribute") { $row.ActiveOwnersCount = ($owners.AdditionalProperties."$activeIdentityAttribute" -match $true).Count }
                else { $row.ActiveOwnersCount = 0 }
                $row.Owners = $owners.AdditionalProperties.userPrincipalName -join ';'
            } else { $row.ActiveOwnersCount = 0 }
        } else { $row.ActiveOwnersCount = 'NA' }
        
        if ($ExportGroupMembers) {
            $rowMembers = $exportMembers.NewRow()
            $rowMembers.GroupId = $mgGroup.Id
            $rowMembers.Owners = $owners.AdditionalProperties.userPrincipalName -join ';'
            $rowMembers.Members = $members.AdditionalProperties.userPrincipalName -join ';'
            $exportMembers.Rows.Add($rowMembers) ### Ulozeni informaci o clenstvi ve skupine do exportni tabulky
        }
    }

    if ($IncludeUsageReports) {
        $groupUsageReport = $mgReportOffice365GroupActivityDetail.($mgGroup.Id)
        foreach ($property in $reportGroupProperties) {
            if ([bool]$groupUsageReport.$property) {
                $row."GRP $property" = $groupUsageReport.$property
            }
        }

        $teamUsageReport = $mgReportTeamActivityDetail.($mgGroup.Id)
        foreach ($property in $reportTeamProperties) {
            if ([bool]$teamUsageReport.$property) {
                $row."TEAM $property" = $teamUsageReport.$property
            }
        }

        ### Pokud neni v proxyaddresses uvedena emailova adresa zacinaciji SPO: pak se SpoSiteID neuvede
        $spoSiteID = ([Regex]::Matches($mgGroup.ProxyAddresses -split "@" -match "SPO:", $guidPattern).Value)
        if ([bool]$spoSiteID) {
            $sharePointSiteUsageReport = $mgReportSharePointSiteUsageDetail.$spoSiteID
            foreach ($property in $reportSharePointProperties) {
                if ([bool]$sharePointSiteUsageReport.$property) {
                    $row."SPO $property" = $sharePointSiteUsageReport.$property
                }
            }
        }
    }

    if ($IncludeEXOMailboxStat -and ($mgGroup.GroupTypes -contains 'Unified')) {
        $mailboxStat = Get-MailboxStatistics -Identity $mgGroup.Id
        $inboxStat = Get-ExoMailboxFolderStatistics -Identity $mgGroup.Id -IncludeOldestAndNewestITems -FolderScope Inbox -ErrorAction SilentlyContinue
        $teamsChatStat = Get-ExoMailboxFolderStatistics -Identity $mgGroup.Id -IncludeOldestAndNewestItems -FolderScope NonIPMRoot -ErrorAction SilentlyContinue | Where-Object {$_.FolderType -eq "TeamsMessagesData"}
        #$oldTeamsChatStat = Get-ExoMailboxFolderStatistics -Identity $mgGroup.Id -IncludeOldestAndNewestItems -FolderScope ConversationHistory  -ErrorAction SilentlyContinue ### Původní umístěni konverzace

        foreach ($property in $mailboxStatProperties) {
            if ([bool]$mailboxStat.$property) {
                $row."MBX $property" = $mailboxStat.$property
            }
        }

        foreach ($property in $mailboxFolderProperties) {
            if ([bool]$inboxStat.$property) {
                $row."MBX INBOX FOLDER $property" = [string]$inboxStat.$property
            }
            if ([bool]$teamsChatStat.$property) {
                $row."MBX TEAM FOLDER $property" = [string]$teamsChatStat.$property
            }
        }
    }

    if ($IncludeAuditLogs -and [bool]$spoSiteID) {
        $unifiedAuditLogRecords = [array](Search-UnifiedAuditLog -RecordType $auditLogRecordType -SiteIds $spoSiteID -StartDate $auditLogStartDate -EndDate $auditLogEndDate -ResultSize $auditLogResultSize) | Where-Object {$_.UserIds -notmatch "app@sharepoint|SHAREPOINT\\system"} | Sort-Object -Property CreationDate -Descending
        if ([bool]$unifiedAuditLogRecords) {
            $row.'SPO AuditLogRecordsCount' = $unifiedAuditLogRecords.Count
            $row.'SPO LastAuditLogDateTime' = $unifiedAuditLogRecords[0].CreationDate
            $row.'SPO UserIds' = $unifiedAuditLogRecords[0].UserIds
            $row.'SPO LastAuditLogRecordType' = "$($unifiedAuditLogRecords[0].RecordType) ($($unifiedAuditLogRecords[0].Operations))"
        }
    }
    
    $exportGroupActivity.Rows.Add($row) ### Ulozeni informaci o skupine do exportni tabulky
}

Write-Host "Exporting Data..."
if ($ExportGroupMembers) { $exportMembers | Export-Csv -Delimiter ',' -Path $ExportMembersFilePath -Encoding UTF8 -NoTypeInformation }

$exportGroupActivity | Export-Csv -Delimiter ',' -Path $ExportFilePath -Encoding UTF8 -NoTypeInformation
