﻿<#
.DESCRIPTION
    Required Modules:      ExchangeOnlineManagement, Microsoft.Graph 

    Minumum Roles or Permissions:
        * ExchangeOnline:  View-Only Audit Logs/Audit Logs/Information Protection Analyst/Information Protection Investigator
                           View-Only Recipients/Mail Recipients
        * GraphApi Scope:  GroupMember.Read.All,Reports.Read.All,Member.Read.Hidden,AuditLog.Read.All
    
    Upozorneni: Do pameti se nacita velke mnozstvi informaci, proto je nutne mit alokovanu dostatecnou kapacitu.
                Napr. 35 000 skupin, 200 000 uzivatelu cca. 20 GB RAM

.PARAMETER ImportFilePath
    Definuje cestu k csv souboru, ktery obsahuje seznam uzivatelu, jejich aktivita ma byt auditovana.
    Uzivatele v souboru mohou byt zadani pomoci UserID, UserPrincipalName, emailove adresy. Na kazdem radku musi byt zadan pouze jeden uzivatel.
    Nelze pouzit soucasne s parametry: Users a UserType
.PARAMETER Users
    Rucne zadany seznam skupin, jejich aktivita ma byt auditovana. Skupiny lze zadat pomoci GroupID nebo DisplayName.
    Skupiny se oddeluji carkou a v pripade, ze DisplayName obsahuje mezeru nebo jiny znak, pak je nutne zadat nazev do uvozovek.
    Nelze pouzit soucasne s parametry: ImportFilePath a GroupType
.PARAMETER UserType
    Definuje filtr, ktery urcuje, jaky typ uzivatetu bude auditovan:
    * All - vsechni uzivatele
    * Guest - hostovane ucty
    * Members - clenske ucty
    Nelze pouzit soucasne s parametry: ImportFilePath a Users
.PARAMETER IncludeUsageReports
    Do vystupu zahrne i informace z UsageReports
.PARAMETER IncludeAuditLogs
    Do vystupu zahrne i informace z auditnich logu
    Prochazeni auditnich logu vyrazne prodluzuje beh skriptu !!!
.PARAMETER IncludeEXOMailboxStat
    Do vystupu zahrne i informace o schrance a o slozce: Inbox
    Zahrnuti techtno informaci do auditu prodluzuje beh skriptu !!!
.PARAMETER IncludeMessageTrace
    Vcetne prochazeni mail logu. Max. 10 dni.
.PARAMETER IncludeMembership
    Do vystupu zahrne aktualni pocet clenu a UPN vsech vlastniku skupiny.
    Nacteni vsech clenu a vlastniku prodluzuje beh scriptu v zavislosti na poctu skupin a mnozstvi clenu.
    Poku je ale definovana promenna $activeIdentityAttribute, pak do vystupu zahrne i pocty aktivnich clenu a vlastniku.
    Promenna $activeIdentityAttribute obsahuje nazev atributu, ktery oznacuje aktivni identity v IS univerzity.
.PARAMETER ExportGroupMembership
    Vyexportuje seznam skupin, kterych je uzivatel clenem.
.PARAMETER ExportDirectoryPath
    Cesta k vystupnimu souboru. Pokud bude zadana neplana, pak se vysledky ulozi do "$($env:HOMEDRIVE)\Temp"

.EXAMPLE
    Export-UserInfo.ps1 -UserType Guest -IncludeUsageReports -ExportDirectoryPath C:\Temp
    Popis:
    Nacte informace pouze o uzivatelych typu host. Informace o aktivite nacte pouze z Usage Reports.
    Vystupem je soubor obsahujici podrobne informace o aktivite vsech hostu.
    Ostatni uzivatele nebudou soucasti reportu.

.EXAMPLE
    Export-UserpInfo.ps1 -ImportFilePath C:\Temp\importUsers.csv -IncludeUsageReports -IncludeAuditLogs -IncludeMessageTrace -IncludeMembership -ExportGroupMembership -ExportDirectoryPath C:\Temp
    Popis:
    Provede nacteni vybranych uzivatelu ze vstupniho souboru a nacte jejich aktivitu ze vsech zdroju.
    Vystupem je soubor obsahujici podrobne informace o aktivite vsech uzivatelu nactenych ze vstupniho souboru a soubor obsahujici informace o tom, jakych skupin je uzivatel clenem.

.NOTES
    Date:   2024-01-30
	Author: Stanislav Kalina, stanislav.kalina@cvut.cz
#>

param(
    [Parameter(Position=0,Mandatory=$true,ParameterSetName='ImportFilePath')][ValidateScript(
    {
        if (!(Test-Path $_ -PathType Leaf)) {throw "Input file ($($_)) does not exist !"}
        if ($_ -notmatch "\.csv$") { throw "The file specified in the path argument must be csv type !" }
        return $true
    })][array]$ImportFilePath,
    [Parameter(Position=0,Mandatory=$true,ParameterSetName='Users')][array]$Users,
    [Parameter(Position=0,Mandatory=$true,ParameterSetName='UserType')][ValidateSet('All','Guest','Member')][string]$UserType,
    [Parameter(Position=1,Mandatory=$false)][switch][bool]$IncludeUsageReports,
    [Parameter(Position=2,Mandatory=$false)][switch][bool]$IncludeAuditLogs,
#    [Parameter(Position=3,Mandatory=$false)][switch][bool]$IncludeEXOMailboxStat,
    [Parameter(Position=4,Mandatory=$false)][switch][bool]$IncludeMessageTrace,
    [Parameter(Position=5,Mandatory=$false)][switch][bool]$ExportGroupMembership,
    [Parameter(Position=6,Mandatory=$false)][ValidateScript(
    {
        if (!(Test-Path $_ -PathType Container)) {throw "Output file ($($_)) does not exist !"}
        return $true
    })][string]$ExportDirectoryPath = "$($env:HOMEDRIVE)\Temp" 
)



[array]$activeIdentityAttribute = @('extension_6511fd4bb0bc4f8694f52b14c8c599ad_cvutPerson') ### Nazev atributu v EntraID, ktery urcuje, jestli ma clen skupiny aktivni vztah k univerzite. Pokud neni nastavena zadna hodnota, pak nebudou ve vysledcich zobrazeny hodnoty ve sloupci ORG_IsActive
[int32]$auditLogResultSize = 100 ### Specifikuje maximalni pocer vysledku z auditniho logu. Maximalni hodnota je 5000.
[int32]$auditLogDays = 180 ### Urcuje, kolik dni do minulosti se maji prohledavat auditni logy
[int32]$messageTraceDays = 10 ### Urcuje, kolik dni do minulosti se maji prohledavat transportni logy. Maximalni hodnota je 10
[string]$excludeGroupFilter = "^All Users$" ### Filtr, ktery urcuje DisplayName skupin, ktere se nebudou analyzovany a nebudou se tak zobrazovat ve vysledcich
[int32]$newUserDays = 7 ### Definuje pocet dni od zalozeni, kdy bude uzivatel v exportu oznacena jako novy


[datetime]$dateTime = Get-Date
[string]$dateTimeFileFormat = Get-Date $dateTime -Format yyyyMMddHHmmss

[datetime]$auditLogStartDate = $dateTime.AddDays(-$auditLogDays)
[datetime]$auditLogEndDate = $dateTime

[datetime]$messageTraceStartDate = $dateTime.AddDays(-$messageTraceDays)
[datetime]$messageTraceEndDate = $dateTime

$ExportDirectoryPath = $ExportDirectoryPath.TrimEnd('\').TrimEnd('/')
$ExportFilePath = "$ExportDirectoryPath\ExportUser-$dateTimeFileFormat.csv"
$ExportMembershipFilePath = "$ExportDirectoryPath\ExportGroupMembership-$dateTimeFileFormat.csv"

[string]$reportActivityPeriod = 'D180' ### Definuje jestli se maji pouzit reporty za poslednich 7, 30, 90 nebo 180 dni. Moznosti: D7/D30/D90/D180
$reportOffice365ActiveUserFilePath = "$ExportDirectoryPath\MgReportOffice365ActiveUserDetail-$dateTimeFileFormat.csv"
$reportEmailActivityUserFilePath = "$ExportDirectoryPath\MgReportEmailActiveUserDetail-$dateTimeFileFormat.csv"
$reportTeamUserActivityFilePath = "$ExportDirectoryPath\MgReportTeamUserActiveUserDetail-$dateTimeFileFormat.csv"
$reportOneDriveActivityUserFilePath = "$ExportDirectoryPath\MgReportOneDriveActiveUserDetail-$dateTimeFileFormat.csv"
$reportSharePointActivityUserFilePath = "$ExportDirectoryPath\MgReportSharePointActiveUserDetail-$dateTimeFileFormat.csv"

### Vlastnosti, ktere budou nacitany a exportovany:
$mgUserProperties = $('Id','UserPrincipalName','DisplayName','UserType','AccountEnabled','OnPremisesSyncEnabled','CreatedDateTime','CreationType','ExternalUserState','ExternalUserStateChangeDateTime','Mail','OtherMails','ProxyAddresses','SignInActivity') ### Get-MgUser
$mgUserSignInProperties = @('LastSignInDateTime','LastNonInteractiveSignInDateTime') ### Get-MgUser
$reportOffice365Properties = $('Report Refresh Date','Exchange Last Activity Date','OneDrive Last Activity Date','SharePoint Last Activity Date','Teams Last Activity Date')
$reportEmailProperties = $('Send Count','Receive Count','Read Count','Meeting Created Count','Meeting Interacted Count')
$reportTeamProperties = $('Team Chat Message Count','Private Chat Message Count','Call Count','Meeting Count','Meetings Organized Count','Meetings Attended Count','Ad Hoc Meetings Organized Count','Ad Hoc Meetings Attended Count','Scheduled One-time Meetings Organized Count','Scheduled One-time Meetings Attended Count','Scheduled Recurring Meetings Organized Count','Scheduled Recurring Meetings Attended Count')
$reportOneDriveProperties = $('Viewed Or Edited File Count','Synced File Count','Shared Internally File Count','Shared Externally File Count')
$reportSharePointProperties = $('Viewed Or Edited File Count','Synced File Count','Shared Internally File Count','Shared Externally File Count','Visited Page Count') ### pridan prefix SPO, abty doslo k odliseni atributu od OneDrive
$groupProperties = $('Onpremise Group Count','OnPremise Groups','Unified Group Count','Unified Groups')
$auditLogProperties = $('AuditLogRecords Count','LastAuditLogDateTime','LastAuditLogRecordType')
$messageTraceProperties = $('MessageTraceRecords Count','LastMessageTraceReceived')
$membershipProperties = @('UserPrincipalName','UserId','OnPremiseGroups','unifiedGroups')




if ([bool]$ImportFilePath) { [array]$Users = Get-Content $ImportFilePath }



$error.Clear()

Write-Host "GraphApi Connection..."
Connect-MgGraph -Scopes GroupMember.Read.All,Reports.Read.All,Member.Read.Hidden,AuditLog.Read.All -NoWelcome
if ($IncludeEXOMailboxStat -or $IncludeAuditLogs -or $IncludeMessageTrace) {
    Write-Host "ExchangeOnline Connection..."
    Connect-ExchangeOnline -ShowBanner:$false
}

if ([bool]$error.Count) { BREAK }




[array]$mgUsers = $()

if ([bool]$Users.Count) {
    foreach ($user in $Users) {
        if ([guid]::TryParse($user, $([ref][guid]::Empty))) { $mgUsers += Get-MgUser -Filter "Id eq '$user'" -Property ($mgUserProperties+$activeIdentityAttribute) -ExpandProperty MemberOf | Select-Object -Property ($mgUserProperties+'AdditionalProperties'+'MemberOf') }
        else { $mgUsers += Get-MgUser -Filter "UserPrincipalName eq '$user' or mail eq '$user'" -Property ($mgUserProperties+$activeIdentityAttribute) -ExpandProperty MemberOf | Select-Object -Property ($mgUserProperties+'AdditionalProperties'+'MemberOf') }
    }
}
else {
    if ($UserType -match 'Guest|Member') { $userTypeFilter = "UserType eq '$UserType'" } else { $userTypeFilter = "" }
    $mgUsers = Get-MgUser -All -Filter $userTypeFilter -Property ($mgUserProperties+$activeIdentityAttribute) -ExpandProperty MemberOf | Select-Object -Property ($mgUserProperties+'AdditionalProperties'+'MemberOf')
}

if ([bool]$mgUsers.Count) { Write-Host "Loaded $($mgUsers.Count) Users !" } else { BREAK }

### Ulozeni a nacteni vsech Usage Reportu
if ($IncludeUsageReports) {
    Get-MgReportOffice365ActiveUserDetail -Period $reportActivityPeriod -OutFile $reportOffice365ActiveUserFilePath
    if ($?) {
        if (Test-Path $reportOffice365ActiveUserFilePath) {
            $mgReportOffice365ActiveUserDetail = @{}
            Import-Csv $reportOffice365ActiveUserFilePath | ForEach-Object { $mgReportOffice365ActiveUserDetail.Add($_."User Principal Name",$_) }
        }
    }

    Get-MgReportEmailActivityUserDetail -Period $reportActivityPeriod -OutFile $reportEmailActivityUserFilePath
    if ($?) {
        if (Test-Path $reportEmailActivityUserFilePath) {
            $mgReportEmailActiveUserDetail = @{}
            Import-Csv $reportEmailActivityUserFilePath | ForEach-Object { $mgReportEmailActiveUserDetail.Add($_."User Principal Name",$_) }
        }
    }
    
    Get-MgReportTeamUserActivityUserDetail -Period $reportActivityPeriod -OutFile $reportTeamUserActivityFilePath
    if ($?) {
        if (Test-Path $reportTeamUserActivityFilePath) {
            $mgReportTeamUserActiveUserDetail = @{}
            Import-Csv $reportTeamUserActivityFilePath | ForEach-Object { $mgReportTeamUserActiveUserDetail.Add($_."User Principal Name",$_) }
        }
    }
    
    Get-MgReportOneDriveActivityUserDetail -Period $reportActivityPeriod -OutFile $reportOneDriveActivityUserFilePath
    if ($?) {
        if (Test-Path $reportOneDriveActivityUserFilePath) {
            $mgReportOneDriveActiveUserDetail = @{}
            Import-Csv $reportOneDriveActivityUserFilePath | ForEach-Object { $mgReportOneDriveActiveUserDetail.Add($_."User Principal Name",$_) }
        }
    }
    
    Get-MgReportSharePointActivityUserDetail -Period $reportActivityPeriod -OutFile $reportSharePointActivityUserFilePath
    if ($?) {
        if (Test-Path $reportSharePointActivityUserFilePath) {
            $mgReportSharePointActiveUserDetail = @{}
            Import-Csv $reportSharePointActivityUserFilePath | ForEach-Object { $mgReportSharePointActiveUserDetail.Add($_."User Principal Name",$_) }
        }
    }
}

$allColumns = @('ORG_NewAccount','ORG_IsActive')
$allColumns += ($mgUserProperties -notmatch 'SignInActivity')
$allColumns += $mgUserSignInProperties
if ($IncludeUsageReports) {
    $allColumns += $reportOffice365Properties
    $reportEmailProperties | foreach { $allColumns += "EXO $($_)" }
    $reportTeamProperties | foreach { $allColumns += "TEAM $($_)" }
    $reportOneDriveProperties | foreach { $allColumns += "1DRV $($_)" }
    $reportSharePointProperties | foreach { $allColumns += "SPO $($_)" }
}
if ($IncludeEXOMailboxStat) {}
if ($IncludeAuditLogs) { $allColumns += $auditLogProperties }
if ($IncludeMessageTrace) { $allColumns += $messageTraceProperties }
$allColumns += $groupProperties

### Zalozeni tabulky, do ktere se budou zapisovat informace o uzivatelich a ktera bude na konci skriptu exportovana do vystupniho souboru
$exportUserActivity = New-Object System.Data.DataTable
foreach ($columnName in $allColumns) { $exportUserActivity.Columns.Add($columnName, 'System.String') | Out-Null }

if ($ExportGroupMembership) {
    ### Zalozeni tabulky, do ktere se budou zapisovat informace o clenech skupin a ktera bude na konci skriptu exportovana do vystupniho souboru
    $exportMembership = New-Object System.Data.DataTable
    foreach ($columnName in $membershipProperties) { $exportMembership.Columns.Add($columnName, 'System.String') | Out-Null }
}


$mgUserNo = 0

foreach ($mgUser in $mgUsers) {

    $mgUserNo++
    $ProgressBar = "Processing $mgUserNo of $($mgUsers.Count): $($mgUser.UserPrincipalName)"  
    Write-Progress -Activity "Checking MgUsers activity:" -Status $ProgressBar -PercentComplete ($mgUserNo/$mgUsers.Count*100)
    
    $row = $exportUserActivity.NewRow()

    if ($mgUser.CreatedDateTime -gt $dateTime.AddDays(-$newUserDays)) { $row.ORG_NewAccount = 'True' }
    if ([bool]$mguser.AdditionalProperties."$activeIdentityAttribute") { $row.ORG_IsActive = 'True' }
     
    foreach ($property in ($mgUserProperties -notmatch 'SignInActivity')) {
        if ([bool]$mgUser.$property) {
            if ($mgUser.$property.GetType().BaseType.Name -eq 'Array') { if ([bool]$mgUser.$property) { $row.$property = $mgUser.$property -join ';' } }
            else { $row.$property = $mgUser.$property }
        }
    }

    foreach ($property in $mgUserSignInProperties) {
        if ([bool]$mgUser.'SignInActivity'.$property) {
            $row.$property = $mgUser.'SignInActivity'.$property
        }
    }
    
    $memberOf = $mgUser.MemberOf 
    if ($memberOf.Count -ge 20) { $memberOf = Get-MgUserMemberOf -UserId $mgUser.Id -All }   
    if ([bool]$memberOf) {
        
        $memberof = $memberof | Where-Object {$_.AdditionalProperties["displayName"] -notmatch $excludeGroupFilter} | Select-Object -ExpandProperty AdditionalProperties

        [array]$onPremiseGroups = $memberOf | Where-Object {$_.onPremisesSyncEnabled}
        [array]$unifiedGroups = $memberOf | Where-Object {$_.groupTypes -eq "Unified"}
    
        $row.'Onpremise Group Count' = $onPremiseGroups.Count
        $row.'OnPremise Groups' = $onPremiseGroups.displayName -join ';'
        $row.'Unified Group Count' = $unifiedGroups.Count
        $row.'Unified Groups' = $unifiedGroups.displayName -join ';'

        if ($ExportGroupMembership) {
            $rowMem = $exportMembership.NewRow()
            $rowMem.UserPrincipalName = $mgUser.UserPrincipalName
            $rowMem.UserId = $mgUser.Id
            $rowMem.unifiedGroups = $unifiedGroups.displayName -join ';'
            $rowMem.OnPremiseGroups = $onPremiseGroups.displayName -join ';'
            $exportMembership.Rows.Add($rowMem)
        }
    }
    

   
    if ($IncludeUsageReports) {
        $userActivityReport = $mgReportOffice365ActiveUserDetail.$($mgUser.UserPrincipalName)
        foreach ($property in $reportOffice365Properties) {
            if ([bool]$userActivityReport.$property) {
                $activityReportDateTime = Get-Date $userActivityReport.$property
                $row.$property = $activityReportDateTime
            }
        }

        $userActivityReport = $mgReportEmailActiveUserDetail.$($mgUser.UserPrincipalName)
        foreach ($property in $reportEmailProperties) {
            if ([bool]$userActivityReport.$property) {
                $row."EXO $property" = $userActivityReport.$property
            }
        }

        $userActivityReport = $mgReportTeamUserActiveUserDetail.$($mgUser.UserPrincipalName)
        foreach ($property in $reportTeamProperties) {
            if ([bool]$userActivityReport.$property) {
                $row."TEAM $property" = $userActivityReport.$property
            }
        }

        $userActivityReport = $mgReportOneDriveActiveUserDetail.$($mgUser.UserPrincipalName)
        foreach ($property in $reportOneDriveProperties) {
            if ([bool]$userActivityReport.$property) {
                $row."1DRV $property" = $userActivityReport.$property
            }
        }

        $userActivityReport = $mgReportSharePointActiveUserDetail.$($mgUser.UserPrincipalName)
        foreach ($property in $reportSharePointProperties) {
            if ([bool]$userActivityReport.$property) {
                $row."SPO $property" = $userActivityReport.$property
            }
        }
    }

   
   
    if ($IncludeAuditLogs) {
        $unifiedAuditLogRecords = [array](Search-UnifiedAuditLog -UserIds $mgUser.Id,$mgUser.Mail,$mgUser.UserPrincipalName -StartDate $auditLogStartDate -EndDate $auditLogEndDate -ResultSize $auditLogResultSize) | Sort-Object -Property CreationDate -Descending
        if ([bool]$unifiedAuditLogRecords) {
            $row.'AuditLogRecords Count' = $unifiedAuditLogRecords.Count
            $row.LastAuditLogDateTime = $unifiedAuditLogRecords[0].CreationDate
            $row.LastAuditLogRecordType = "$($unifiedAuditLogRecords[0].RecordType) ($($unifiedAuditLogRecords[0].Operations))"
        }
    }

   
   
    if ($IncludeMessageTrace) {
        $messageTraceRecords = [array](Get-MessageTrace -StartDate $messageTraceStartDate -EndDate $messageTraceEndDate -Recipient $mgUser.Mail) | Sort-Object -Property Received -Descending
        if ([bool]$messageTraceRecords) {
            $row.'MessageTraceRecords Count' = $messageTraceRecords.Count
            $row.LastMessageTraceReceived = $messageTraceRecords[0].Received
        } 
    }

    $exportUserActivity.Rows.Add($row)

}

Write-Host "Exporting Data..."
if ($ExportGroupMembership) { $exportMembership | Export-Csv -Delimiter ',' -Path $ExportMembershipFilePath -Encoding UTF8 -NoTypeInformation }

$exportUserActivity | Export-Csv -Delimiter ',' -Path $ExportFilePath -Encoding UTF8 -NoTypeInformation
